#!/bin/bash

function copy_reverse_proxy_conf {
    cat /etc/nginx/conf.d/server.ReverseProxy.conf \
    | perl -0pe 's/server \{\n    listen (?!443)(.|\n)+?\n\}/# SERVER REMOVED/g' \
    | sed -r '
        s/listen (\[::\]:)?443 ssl;/listen \19080;/;
        s/include .*(certificate|security-profile)/# \0/;
        s/include conf.d\/\.acl/# \0/;
        s/proxy_ssl_protocols/# \0/;
        s/add_header Strict-Transport-Security/# \0/;
        s/location \/ \{/include conf.d\/sso.conf;\n\n    \0/;
        ' \
     > /etc/nginx/sites-enabled/server.ReverseProxy.conf
}
