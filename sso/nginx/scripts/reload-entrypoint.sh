#!/bin/bash

source /scripts/include.sh

apt-get update && \
    apt-get install -y inotify-tools && \
    apt-get clean

mkdir /etc/nginx/sites-enabled
copy_reverse_proxy_conf

sh -c /scripts/reload.sh &
