#!/bin/bash

source /scripts/include.sh

while inotifywait --exclude .swp -e create -e modify -e delete -e move --fromfile /scripts/reload-files.txt; do
    copy_reverse_proxy_conf

    if nginx -t; then
        echo "=== Reloading nginx ==="
        nginx -s reload
    fi
done