<?php

// --- Config ---

$KEY = '<your key from sso/vouch/secret>';
$DOMAIN = '<yourdomain.com>';
$ADMIN = '<your username>';

$COOKIE = 'VouchCookie';
$USER_TTL = 4 * 3600;
$API_TTL = 365 * 86400;
$LOGIN_URL = "https://vouch.$DOMAIN/login?url=https://$DOMAIN/token.php";
$LOGOUT_URL = "https://$DOMAIN:5001/webapi/entry.cgi?api=SYNO.API.Auth&version=7&method=logout&redirect_url=https://$DOMAIN/token.php";

// --- Main ---

list($token, $head, $body) = decode($encoded = $_COOKIE[$COOKIE]);

switch($action = @$_GET['action']) {
    case 'clear':
    case 'logout':
        unset($_COOKIE[$COOKIE], $encoded, $token, $head, $body);
        setcookie($COOKIE, '', -1, '/', ".$DOMAIN");
        if ($action !== 'logout') break;
        header("Location: $LOGOUT_URL", true, 302);
        die();
    case 'api':
        if ($body?->username !== $ADMIN) break;
        $body->exp = time() + $API_TTL;
        list($token, $head, $body) = decode($encoded = encode($head, $body));
        break;
}

// --- HTML ---

?>
<html>
    <head>
        <style type="text/css">
            :root {
                --background: #2c4251ff;
                --panels: #d16666ff;
                --links: #ffb703;
                --text: #ffffffff;
            }

            body {
                font-family: sans-serif;
                background-color: var(--background);
                color: var(--text);
            }

            h1, div {
                margin: 50px auto;
            }

            p, pre, a {
                font-size: 16px;
            }

            a {
                color: var(--links);

                &:hover {
                    filter: brightness(120%);
                }
            }

            h1, p:has(a) {
                text-align: center;
            }

            div {
                max-width: 1024px;
                overflow-wrap: break-word;
                border: 3px solid black;
                background: var(--panels);
                padding-inline: 10px;
                padding-top: 0;
            }
        </style>
        <script type="text/javascript">
            function select(el) {
                const range = document.createRange();
                range.selectNodeContents(el);
                
                const selection = window.getSelection();
                selection.removeAllRanges();
                selection.addRange(range);
            }
        </script>
    </head>
    <body>
        <h1><?= !$body ? 'No' : ($body->exp > time() + $USER_TTL ? 'API' : 'User') ?> token</h1>

        <? if ($encoded) : ?>
            <div>
                <p><b>Encoded</b></p>
                <p onclick="select(this)"><?= $encoded ?></p>
            </div>
        <? endif ?>

        <? if($token) : ?>
            <div>
                <p><b>JWT</b></p>
                <p onclick="select(this)"><?= $token ?></p>
            </div>
        <? endif ?>

        <? if ($body) :
            $exp = date_create(date("Y-m-d H:i:s", $body->exp));
            $ttl = date_diff($exp, date_create());
            ?>
            <div>
                <p><b>Data</b></p>
                <pre onclick="select(this)">const head = <?= json_encode($head, JSON_PRETTY_PRINT) ?></pre>
                <pre onclick="select(this)">const body = <?= json_encode($body, JSON_PRETTY_PRINT) ?></pre>
                <p>Expiration: <?= $exp->format("Y-m-d H:i:s") ?> (<?= $ttl->format("%a days %H:%I:%S hours") ?>)</p>
            </div>
        <? endif ?>
        
        <p>
            <a href="<?= $LOGIN_URL ?>">Get token</a>

            <? if($body) : ?>
                | <a href="?action=clear">Clear token</a>
                | <a href="?action=logout">Logout</a>
            <? endif ?>
        </p>

        <? if($body?->username === $ADMIN) : ?>
            <p>
                <a href="?action=api">Generate API token</a>
            </p>
        <? endif ?>
    </body>
</html>
<?

// --- JWT ---

function sign($msg) {
    global $KEY;

    return hash_hmac('SHA256', $msg, $KEY, true);
}

function verify($msg, $sig) {
    return hash_equals($sig, sign($msg));
}

function encode($head, $body) {
    $head = base64url_encode(json_encode($head));
    $body = base64url_encode(json_encode($body));
    $msg = "$head.$body";
    $sig = base64url_encode(sign($msg));
    return base64url_encode(gzencode("$msg.$sig"));
}

function decode($token) {
    global $DOMAIN;

    $token = gzdecode(base64url_decode($token));
    if (substr_count($token, '.') !== 2) return;

    list($head, $body, $sig) = array_map('base64url_decode', explode('.', $token));

    $msg = substr($token, 0, strrpos($token, '.'));
    if (!verify($msg, $sig)) return;

    $head = json_decode($head);
    if ($head?->typ !== "JWT" || $head->alg !== 'HS256') return;

    $body = json_decode($body);
    if ($body?->iss !== 'Vouch' || $body->aud !== $DOMAIN || !$body->exp) return;
    if ($body->exp < time()) return;

    return [$token, $head, $body];
}

// --- Util ---

function base64url_decode($val) {
    return base64_decode(strtr($val, '-_', '+/'));
}

function base64url_encode($val) {
    return rtrim(strtr(base64_encode($val), '+/', '-_'), "=");
}